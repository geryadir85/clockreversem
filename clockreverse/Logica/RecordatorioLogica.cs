﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using clockreverse.Datos;

namespace clockreverse.Logica
{
   public class RecordatorioLogica
    {
        public void LeerRecordatorio()
        {
            string filepath = @"C:\clokmilitar\clockmilitar.frf";
            string line;

            StreamReader sr = new StreamReader(filepath);
           
            List<Recordatorios> list = new List<Recordatorios>();





            //Read the first line of text
            line = sr.ReadLine();

            //Continue to read until you reach end of file

            while (line != null)
            {
                //srl.ReadLine();

                if (!String.IsNullOrEmpty(line))
                {

                    Recordatorios recordatorio = new Recordatorios();
                    if (line.Contains("Recordatorio"))
                    {
                        recordatorio.Recordatorio = line.Split(',').Last().ToString();

                    }
                    line = sr.ReadLine();
                    if (line.Contains("Hora"))
                    {
                        recordatorio.Hora = Convert.ToDateTime(line.Split('/').Last().ToString());
                        //recordatorio.Hora = line.Split(',').Last().ToString();
                    }
                    line = sr.ReadLine();
                    if (line.Contains("Fecha"))
                    {
                        recordatorio.Fecha = Convert.ToDateTime(line.Split('/').Last().ToString());
                        //recordatorio.Fecha = line.Split(',').Last().ToString();
                    }

                    list.Add(recordatorio);
                }
                line = sr.ReadLine();
            }

            if (list.Count > 0)
            {
                foreach (Recordatorios rec in list)
                {

                    if (rec.Hora.ToLongTimeString() == DateTime.Now.ToLongTimeString() && rec.Fecha.ToShortDateString() == DateTime.Now.ToShortDateString())
                    {

                        MessageBox.Show(rec.Recordatorio, "Recordatorio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }



            }

            //close the file
            sr.Close();


        }


        public void GuardarRecordatorio(string recordatorio, DateTime fecha, DateTime hora)
        {


            string filepath = @"C:\clokmilitar\clockmilitar.frf", directorypath = @"C:\clokmilitar";




            if (Directory.Exists(directorypath))
            {



                StreamWriter sw = new StreamWriter(filepath, true);

                sw.WriteLine(System.Environment.NewLine);
                sw.WriteLine("Recordatorio," + recordatorio);


                sw.WriteLine("Hora," + hora.ToLongTimeString());
                sw.WriteLine("Fecha," + fecha.ToShortDateString());


                sw.Close();

            }

            else
            {

                DirectoryInfo di = Directory.CreateDirectory(directorypath);

                StreamWriter sw = new StreamWriter(filepath);

                sw.WriteLine("Recordatorio," + recordatorio);


                sw.WriteLine("Hora," + hora.ToLongTimeString());
                sw.WriteLine("Fecha," + fecha.ToShortDateString());


                sw.Close();


            }


        }


        public List<Recordatorios>ListarTodosRecordatorios()
        {

            string filepath = @"C:\clokmilitar\clockmilitar.frf";
            string line;

            StreamReader sr = new StreamReader(filepath);

            List<Recordatorios> list = new List<Recordatorios>();
            
            line = sr.ReadLine();


            while (line != null)
            {
               

                if (!String.IsNullOrEmpty(line))
                {

                    Recordatorios recordatorio = new Recordatorios();
                    if (line.Contains("Recordatorio"))
                    {
                        recordatorio.Recordatorio = line.Split(',').Last().ToString();

                    }
                    line = sr.ReadLine();
                    if (line.Contains("Hora"))
                    {
                        recordatorio.Hora = Convert.ToDateTime(line.Split(',').Last().ToString());
                    
                    }
                    line = sr.ReadLine();
                    if (line.Contains("Fecha"))
                    {
                        recordatorio.Fecha = Convert.ToDateTime(line.Split(',').Last().ToString());
                     
                    }

                    list.Add(recordatorio);
                }
                line = sr.ReadLine();
            }

            return list;
        }




    }
}
