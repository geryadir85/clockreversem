﻿using System;
using System.Collections;
using System.Threading;
using System.Diagnostics;

/*Nota:
    Si deseas saber mas sobre shutdown(SHUTDOWN.EXE) te recomiendo que abras una ventana de consola
    y ejecutes el comando y te mostrara la lista de las opciones de apagado.
    Aquí solo te muestro unas de las más importantes.
 
    Usage: shutdown [/i | /l | /s | /r | /g | /a | /h | /e] [/m \\computer][/t xxx][/d [p|u:]xx:yy ]
    /i Muestra la interface de usuario (GUI).
    /l Log off.
    /s Apaga la computadora.
    /r Apaga y reinicia la computadora.
    /g Apaga y reinicia la computadora. Usada para registrar aplicaciones.
    /a Aborta un apagado de sistema.
    /h Hibernar la computadora.
    /e Documenta la razon por la cual se apagara la computadora.
    /m \\computer Especifica el target de la computadora a apagar.
    /t xxx El periodo en que se apagar la computadora el cual se encuentre entre 0-600. Tiene por defecto asignado 30 segundos.
    /d [p|u:]xx:yy Provee la razón por la que se reinicia ó apaga el PC.
    (p indica que es planeado. u indica que es planeado por el usuario.)
*/

namespace clockreverse.Utiles
{
    public class ShutDown
    {
        string argumento = null;
        DateTime tmp;
        #region Constructors

        public ShutDown(string argumento, DateTime tmp)
        {
            this.argumento = argumento;
            this.tmp = tmp;
        }

        #endregion

        #region Functions

        public void Shut_Down()
        {
            try
            {
                while (true)
                {
                    if (tmp.ToLongTimeString() == DateTime.Now.ToLongTimeString())
                    {
                        Process proceso = new Process();
                        proceso.StartInfo.UseShellExecute = false;
                        proceso.StartInfo.RedirectStandardOutput = true;
                        proceso.StartInfo.FileName = "shutdown.exe";
                        proceso.StartInfo.Arguments = this.argumento;
                        proceso.Start();
                        break;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion



    }
}
