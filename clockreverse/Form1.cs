﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using clockreverse;

namespace clockreverse
{
    public partial class Form1 : Form
    {
        int hour = 0, minute = 0, seconds = 0;
        string strhour="00", strminute="00", strseconds="00";
        
        
        #region Events
      
        #region TextBoxs
        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.ForeColor = Color.Black;
            textBox1.Text = string.Empty;

        }


        private void textBox2_Click(object sender, EventArgs e)
        {
            textBox2.ForeColor = Color.Black;
            textBox2.Text = string.Empty;
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox2.Text))
            {
                textBox2.ForeColor = Color.Silver;
                textBox2.Text = "mm";
            }
        }

        
        private void textBox3_Click(object sender, EventArgs e)
        {
            textBox3.ForeColor = Color.Black;
            textBox3.Text = string.Empty;
        }

        private void textBox3_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox3.Text))   
            {
                textBox3.ForeColor = Color.Silver;
                textBox3.Text = "ss";
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

            e.Handled = handledController(e);
            
        }

      
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //Evita que se realice validación al textbox cuando esta vacio o con la mascara de prompt  
            if (textBox1.Text != "hh" && !String.IsNullOrEmpty(textBox1.Text))
            {
                textboxClear(sender, 23, out bool argexceds);

                if (argexceds)
                {
                    textBox1.Clear();
                    return;

                }

                else
                {
                    strhour = textBox1.Text;
                    labelWrite(strhour);
                }
            }
         
           


        }

        private void textBox1_Leave(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(textBox1.Text))
            {
                textBox1.ForeColor = Color.Silver;
                textBox1.Text = "hh";
            }
        }


        private void textBox2_TextChanged(object sender, EventArgs e)
        {
           

            if (textBox2.Text != "hh" && !String.IsNullOrEmpty(textBox2.Text))
            {
                textboxClear(sender, 59, out bool argexceds);

                if (argexceds)
                {
                    textBox2.Clear();
                    return;

                }
                        
                else
                {
                    strminute = textBox2.Text;
                    labelWrite(strhour,strminute);
                }
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text != "hh" && !String.IsNullOrEmpty(textBox3.Text))
            {
                textboxClear(sender, 59, out bool argexceds);

                if (argexceds)
                {
                    textBox3.Clear();
                    return;

                }

                else
                {
                    strseconds = textBox3.Text;
                    labelWrite(strhour,strminute,strseconds);
                }
            }
        }

        #endregion

        #region Buttons

        private void button1_Click(object sender, EventArgs e)
        {
            timer_s.Enabled = false;
            unlocketexboxs();
            
           

        }

        private void btnset_Click(object sender, EventArgs e)
        {
            timer_s.Enabled = true;
            locktextboxs();

        }

        private void button2_Click(object sender, EventArgs e)
        {



            clockMilitar.frmOptions frm = new clockMilitar.frmOptions();
            Form1 form1 = new Form1();
            frm.ShowDialog();

        }

        #endregion

        #region Timers
        private void timer_s_Tick(object sender, EventArgs e)
        {

            //Asignacion los valores de las variables string proveniente del label
            hour = Convert.ToInt16(strhour);
            minute = Convert.ToInt16(strminute);
            seconds = Convert.ToInt16(strseconds);
          
            seconds = seconds + 1;
           
            //strhour = hour.ToString();
            //strminute = minute.ToString();
            strseconds = seconds.ToString();
            

            //Contar hasta 59 y luego reiniciar en 00
            if (seconds > 59)
            {
                strseconds = "0";
                minute = minute + 1;
                strminute = minute.ToString();
                seconds = Convert.ToInt16(strseconds);
               
            }
            //Contar hasta 59 y luego reiniciar en 00
            if (minute > 59)
            {
                strminute = "0";
                hour = hour + 1;
                strhour = hour.ToString();
                minute = Convert.ToInt16(strminute);
              
            }
            //Contar hasta 23 y luego reiniciar en 00
            if (hour > 23)
            {
                strhour = "0";
                hour = Convert.ToInt16(strhour);
                strminute = "0";
                strseconds = "0";

            }





            lbltime.Text = hour.ToString("00") + ":" + minute.ToString("00") + ":" + seconds.ToString("00");
            clockreverse.Utiles.Globales.dt = Convert.ToDateTime(lbltime.Text);


        }

        #endregion
        #endregion

        #region Constructors
        public Form1()
        {
            InitializeComponent();



        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = handledController(e);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = handledController(e);

         
        }

       
        #endregion

        #region Functions


        private void labelTo00(object sender)
        {

            Label lbl = sender as  Label;


            lbl.Text = "00:00:00";
            
        }

        private void locktextboxs()
        {

            List<TextBox> textBoxes = new List<TextBox>
            {
                textBox1,
                textBox2,
                textBox3
            };



            textBoxes.ForEach(x => x.Enabled = false);
            textBoxes.Clear();
           
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
                notifyIcon.Visible = true;
            }
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon.Visible = false;
        }

        private void unlocketexboxs()
        {

            List<TextBox> textBoxes = new List<TextBox>
            {
                textBox1,
                textBox2,
                textBox3
            };



            textBoxes.ForEach(x => x.Enabled = true);
            textBoxes.Clear();




        }

        private void labelWrite(string strh, string strm ="00", string strs ="00")
        {
            if (strh.Length == 1)
                strh = "0" + strh;

            if (strm.Length == 1)
                strm = "0" + strm;

            if (strs.Length == 1)
                strs = "0" + strs;

            lbltime.Text = strh + ":" + strm + ":" + strs;

        }

        private void textboxClear(object sender, int limite, out bool exceds )
        {
            /*  ======================================================================
             *  Metodo que evita ingresar numeros enteros mayores a su limite        *
             *  considerando que el dia se divide en 23 horas, 59 minutos y 59       *
             *  segundos.                                                            *
             *  ======================================================================                         
            */

            TextBox textBox = sender as TextBox;

            exceds = false;

            if (!String.IsNullOrEmpty(textBox.Text))
            {
                int j = Convert.ToInt16(textBox.Text);
                if (j > limite)
                {

                    MessageBox.Show("No puede ingresar un valor mayor a " + limite, "Error de logica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    exceds = true;

                }


            }


        }

        private bool handledController(KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                
               return  true;
            }


            return false;
        }

        #endregion
    }
}
