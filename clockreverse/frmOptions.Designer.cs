﻿namespace clockMilitar
{
    partial class frmOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOptions));
            this.dtshutdownsystem = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtrecordatorifecha = new System.Windows.Forms.DateTimePicker();
            this.chkboxUsarMilitarClock = new System.Windows.Forms.CheckBox();
            this.dtrecordatori = new System.Windows.Forms.DateTimePicker();
            this.radioRecordatorio = new System.Windows.Forms.RadioButton();
            this.radioshutdown = new System.Windows.Forms.RadioButton();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.timershutdown = new System.Windows.Forms.Timer(this.components);
            this.btnCancelarOpcion = new System.Windows.Forms.Button();
            this.timerRecordatorio = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.textrecordatory = new System.Windows.Forms.TextBox();
            this.bindingRecordatorio = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingRecordatorio)).BeginInit();
            this.SuspendLayout();
            // 
            // dtshutdownsystem
            // 
            this.dtshutdownsystem.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtshutdownsystem.Location = new System.Drawing.Point(141, 23);
            this.dtshutdownsystem.Name = "dtshutdownsystem";
            this.dtshutdownsystem.ShowUpDown = true;
            this.dtshutdownsystem.Size = new System.Drawing.Size(101, 20);
            this.dtshutdownsystem.TabIndex = 3;
            this.dtshutdownsystem.Value = new System.DateTime(2018, 11, 20, 16, 43, 15, 0);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textrecordatory);
            this.groupBox1.Controls.Add(this.dtrecordatorifecha);
            this.groupBox1.Controls.Add(this.chkboxUsarMilitarClock);
            this.groupBox1.Controls.Add(this.dtrecordatori);
            this.groupBox1.Controls.Add(this.radioRecordatorio);
            this.groupBox1.Controls.Add(this.radioshutdown);
            this.groupBox1.Controls.Add(this.dtshutdownsystem);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Location = new System.Drawing.Point(9, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 138);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Acciones personalizadas";
            // 
            // dtrecordatorifecha
            // 
            this.dtrecordatorifecha.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingRecordatorio, "Fecha", true));
            this.dtrecordatorifecha.Enabled = false;
            this.dtrecordatorifecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtrecordatorifecha.Location = new System.Drawing.Point(314, 57);
            this.dtrecordatorifecha.Name = "dtrecordatorifecha";
            this.dtrecordatorifecha.Size = new System.Drawing.Size(101, 20);
            this.dtrecordatorifecha.TabIndex = 7;
            // 
            // chkboxUsarMilitarClock
            // 
            this.chkboxUsarMilitarClock.AutoSize = true;
            this.chkboxUsarMilitarClock.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkboxUsarMilitarClock.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.chkboxUsarMilitarClock.Location = new System.Drawing.Point(255, 24);
            this.chkboxUsarMilitarClock.Name = "chkboxUsarMilitarClock";
            this.chkboxUsarMilitarClock.Size = new System.Drawing.Size(180, 17);
            this.chkboxUsarMilitarClock.TabIndex = 6;
            this.chkboxUsarMilitarClock.Text = "Usar militarclock como reloj base";
            this.chkboxUsarMilitarClock.UseVisualStyleBackColor = true;
            this.chkboxUsarMilitarClock.CheckStateChanged += new System.EventHandler(this.chkboxUsarMilitarClock_CheckStateChanged);
            // 
            // dtrecordatori
            // 
            this.dtrecordatori.CustomFormat = "HH:mm:ss";
            this.dtrecordatori.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingRecordatorio, "Hora", true));
            this.dtrecordatori.Enabled = false;
            this.dtrecordatori.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtrecordatori.Location = new System.Drawing.Point(314, 80);
            this.dtrecordatori.Name = "dtrecordatori";
            this.dtrecordatori.ShowUpDown = true;
            this.dtrecordatori.Size = new System.Drawing.Size(101, 20);
            this.dtrecordatori.TabIndex = 5;
            this.dtrecordatori.Value = new System.DateTime(2018, 11, 20, 16, 43, 15, 0);
            // 
            // radioRecordatorio
            // 
            this.radioRecordatorio.AutoSize = true;
            this.radioRecordatorio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioRecordatorio.Location = new System.Drawing.Point(6, 57);
            this.radioRecordatorio.Name = "radioRecordatorio";
            this.radioRecordatorio.Size = new System.Drawing.Size(86, 17);
            this.radioRecordatorio.TabIndex = 1;
            this.radioRecordatorio.TabStop = true;
            this.radioRecordatorio.Text = "Recordatorio";
            this.radioRecordatorio.UseVisualStyleBackColor = true;
            this.radioRecordatorio.CheckedChanged += new System.EventHandler(this.radioRecordatorio_CheckedChanged_1);
            // 
            // radioshutdown
            // 
            this.radioshutdown.AutoSize = true;
            this.radioshutdown.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.radioshutdown.Location = new System.Drawing.Point(6, 23);
            this.radioshutdown.Name = "radioshutdown";
            this.radioshutdown.Size = new System.Drawing.Size(108, 17);
            this.radioshutdown.TabIndex = 0;
            this.radioshutdown.TabStop = true;
            this.radioshutdown.Text = "Apagar el sistema";
            this.radioshutdown.UseVisualStyleBackColor = true;
            this.radioshutdown.CheckedChanged += new System.EventHandler(this.radioshutdown_CheckedChanged);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAceptar.Location = new System.Drawing.Point(368, 202);
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(2);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(80, 28);
            this.btnAceptar.TabIndex = 6;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // timershutdown
            // 
            this.timershutdown.Interval = 1000;
            this.timershutdown.Tick += new System.EventHandler(this.timershutdown_Tick);
            // 
            // btnCancelarOpcion
            // 
            this.btnCancelarOpcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelarOpcion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnCancelarOpcion.Location = new System.Drawing.Point(259, 202);
            this.btnCancelarOpcion.Name = "btnCancelarOpcion";
            this.btnCancelarOpcion.Size = new System.Drawing.Size(104, 28);
            this.btnCancelarOpcion.TabIndex = 7;
            this.btnCancelarOpcion.Text = "Cancelar opcion";
            this.btnCancelarOpcion.UseVisualStyleBackColor = true;
            this.btnCancelarOpcion.Click += new System.EventHandler(this.btnCancelarOpcion_Click);
            // 
            // timerRecordatorio
            // 
            this.timerRecordatorio.Enabled = true;
            this.timerRecordatorio.Interval = 1000;
            this.timerRecordatorio.Tick += new System.EventHandler(this.timerRecordatorio_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.bindingNavigator1);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.btnAceptar);
            this.groupBox3.Controls.Add(this.btnCancelarOpcion);
            this.groupBox3.Location = new System.Drawing.Point(61, 63);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(508, 245);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.BindingSource = this.bindingRecordatorio;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.None;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.bindingNavigator1.Location = new System.Drawing.Point(9, 160);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(257, 25);
            this.bindingNavigator1.TabIndex = 8;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Agregar nuevo";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de elementos";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Eliminar";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primero";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posición";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posición actual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover siguiente";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // textrecordatory
            // 
            this.textrecordatory.Location = new System.Drawing.Point(142, 60);
            this.textrecordatory.Multiline = true;
            this.textrecordatory.Name = "textrecordatory";
            this.textrecordatory.Size = new System.Drawing.Size(166, 59);
            this.textrecordatory.TabIndex = 10;
            // 
            // bindingRecordatorio
            // 
            this.bindingRecordatorio.DataSource = typeof(clockreverse.Datos.Recordatorios);
            // 
            // frmOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(612, 352);
            this.Controls.Add(this.groupBox3);
            this.Location = new System.Drawing.Point(630, 150);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Programe las opciones";
            this.Load += new System.EventHandler(this.frmOptions_Load);
            this.SizeChanged += new System.EventHandler(this.frmOptions_SizeChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingRecordatorio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dtshutdownsystem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioRecordatorio;
        private System.Windows.Forms.RadioButton radioshutdown;
        private System.Windows.Forms.DateTimePicker dtrecordatori;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Timer timershutdown;
        private System.Windows.Forms.Button btnCancelarOpcion;
        private System.Windows.Forms.CheckBox chkboxUsarMilitarClock;
        private System.Windows.Forms.DateTimePicker dtrecordatorifecha;
        private System.Windows.Forms.Timer timerRecordatorio;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.BindingSource bindingRecordatorio;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.TextBox textrecordatory;
    }
}