﻿using clockreverse.Utiles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using clockreverse.Datos;
using System.Threading.Tasks;
using System.Windows.Forms;
using clockreverse.Logica;


namespace clockMilitar
{
    public partial class frmOptions : Form
    {
       
        Boolean useglobaltime = false;
        #region Constructors

        public frmOptions()
        {
            InitializeComponent();
        }

       

        #endregion
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioRecordatorio_CheckedChanged(object sender, EventArgs e)
        {
           
        }



        private void radioRecordatorio_CheckedChanged_1(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton.Checked)
           {
                unlockctrlsrecordatorio(); }
            else
            {
                lockctrlrecordatorio();            }

        }

        #region funciones
        private void unlockctrlsrecordatorio()
        {
            textrecordatory.Enabled = true;
            dtrecordatori.Enabled = true;
            dtrecordatorifecha.Enabled = true;
            dtshutdownsystem.Enabled = false;
            chkboxUsarMilitarClock.Enabled = false;


        }

        private void lockctrlrecordatorio()
        {
            textrecordatory.Enabled = false;
            dtrecordatori.Enabled = false;
            dtrecordatorifecha.Enabled = false;
            dtshutdownsystem.Enabled = true;
            chkboxUsarMilitarClock.Enabled = true;

        }

        public void Shut_Down(DateTime tmp, string argumento)
        {
            try
            {
                Process proceso = new Process();
                proceso.StartInfo.UseShellExecute = false;
                proceso.StartInfo.RedirectStandardOutput = true;
                proceso.StartInfo.FileName = "shutdown.exe";
                proceso.StartInfo.Arguments = argumento;


                if (useglobaltime)
                {
                    if (tmp.ToLongTimeString() == Globales.dt.ToLongTimeString())
                    {
                        proceso.Start();
                    }

                }
                else
                {
                    if (tmp.ToLongTimeString() == DateTime.Now.ToLongTimeString())
                    {

                        proceso.Start();

                    }

                }
            }
            catch
            {
                throw;
            }
        }

        public void ScheduleRecordatory(DateTime time, DateTime date, string message)
        {
            if(time.ToLongTimeString() == DateTime.Now.ToLongTimeString() && date.ToShortDateString() == DateTime.Now.ToShortDateString())
            {

                MessageBox.Show(message, "Recordatorio", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }



        #endregion

        private void radioshutdown_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;

            if (radioButton.Checked)
            {
                lockctrlrecordatorio();
               
            }
            else
            {
                unlockctrlsrecordatorio();
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
          



            if (radioshutdown.Checked)
            {
                timershutdown.Enabled = true;
               

            }

            else if(radioRecordatorio.Checked)
            {
                timerRecordatorio.Enabled = true;
               
            }


        }





        private void timershutdown_Tick(object sender, EventArgs e)
        {
            Shut_Down(dtshutdownsystem.Value, "/s");
        }

        private void btnCancelarOpcion_Click(object sender, EventArgs e)
        {
            if (radioshutdown.Checked)
            {
                timershutdown.Enabled = false;
                

            }

            else if(radioRecordatorio.Checked)
            {
                timerRecordatorio.Enabled = false;
               
            }
               


        }

        private void chkboxUsarMilitarClock_CheckStateChanged(object sender, EventArgs e)
        {
            var chekbox = sender as CheckBox;

            if (chekbox.Checked)
            {
                useglobaltime = true;
                return;
            }
            useglobaltime = false;

        }

        private void timerRecordatorio_Tick(object sender, EventArgs e)
        {
            new RecordatorioLogica().LeerRecordatorio();


        }

        private void frmOptions_SizeChanged(object sender, EventArgs e)
        {

        }

        private void btnProgramar_Click(object sender, EventArgs e)
        {
            new RecordatorioLogica().GuardarRecordatorio(textrecordatory.Text, dtrecordatori.Value, dtrecordatorifecha.Value);

        }

        private void frmOptions_Load(object sender, EventArgs e)
        {
            List<Recordatorios> list = new RecordatorioLogica().ListarTodosRecordatorios();

            this.bindingRecordatorio.DataSource = list;
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textrecordatory.Text))
            {
                string d = textrecordatory.Text;

                MessageBox.Show("Debe ingresar un recordatorio","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            
            new RecordatorioLogica().GuardarRecordatorio(textrecordatory.Text,dtrecordatorifecha.Value,dtrecordatori.Value);

        }
    }
}
